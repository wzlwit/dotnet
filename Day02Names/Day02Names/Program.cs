﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Names
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> nameList = new List<string>();
            while (true)
            {
                Console.Write("Enter a name, press enter to quit entering:");
                string input = Console.ReadLine().Trim();
                if (String.Compare(input, "") == 0)
                {
                    Console.WriteLine("\nThe names are:");
                    break;
                }
                else
                {
                    nameList.Add(input);
                }
            }
            foreach (string str in nameList)
            {
                Console.WriteLine(str);
            }
            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
}
