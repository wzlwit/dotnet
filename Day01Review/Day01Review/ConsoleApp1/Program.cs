﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        // part 3, using method to replace the catch-try block
        public static int GetInt(string msg)
        {
            bool intCheck = true;
            int outPut=0;
            while (intCheck)
            {
                Console.Write(msg);
                intCheck = !Int32.TryParse(Console.ReadLine(), out outPut);
                if (intCheck) {
                    Console.WriteLine("It is not a valid number, please try again: ");
                }
            }
            return outPut;
        }

        static void Main(string[] args)
        {
            /** part1:
                Console.Write("Enter minimum: ");
                int min = int.Parse(Console.ReadLine());
                Console.Write("Enter maximum: ");
                int max = int.Parse(Console.ReadLine());
                Console.Write("Enter how many numbers to generate: ");
                int count = int.Parse(Console.ReadLine());
                Random rand = new Random();

                Console.Write("Numbers generated: ");
                for (int i = 0; i < count; i++)
                {
                    Console.Write(" {0}", rand.Next(min, max + 1));
                }

                Console.ReadKey(); 
            */

            //* part2:
            bool intCheck = true, rangeCheck = true;
            int min = 0, max = 0, count = 0;

            while (rangeCheck)
            {
                Console.Write("Enter minimum: ");

                while (intCheck)
                {
                    try
                    {
                        min = int.Parse(Console.ReadLine());
                        intCheck = false;
                    }
                    catch (Exception e)
                    {
                        Console.Write("Please enter an integer as the minimum number: \n");
                        // Console.WriteLine(e);
                        intCheck = true;
                    }
                    /* finally{} */
                }

                intCheck = true;
                Console.Write("Enter maximum: ");
                while (intCheck)
                {
                    try
                    {
                        max = int.Parse(Console.ReadLine());
                        intCheck = false;
                        // Console.WriteLine("the maximum is: {0}", max);

                    }
                    catch (Exception e)
                    {
                        Console.Write("Please enter an integer as the maximum number: \n");
                        // Console.WriteLine(e);
                        intCheck = true;
                    }
                }

                intCheck = true;
                Console.Write("Enter how many numbers to generate: ");
                while (intCheck)
                {
                    try
                    {
                        count = int.Parse(Console.ReadLine());
                        intCheck = false;
                        // Console.WriteLine("the maximum is: {0}", max);

                    }
                    catch (Exception e)
                    {
                        Console.Write("Please enter an integer as the count number: \n");
                        // Console.WriteLine(e);
                        intCheck = true;
                    }
                }

                if (min < max)
                {
                    rangeCheck = false;
                }
                else
                {
                    Console.WriteLine("The minimum must be less than the maximum, please enter any key to re-enter the values");
                    intCheck = true;
                    string mis = Console.ReadLine();
                    // Environment.Exit(1);
                }
            }

            Random rand = new Random();
            Console.Write("Numbers generated: ");
            for (int i = 0; i < count; i++)
            {
                Console.Write("{0}{1}", i == 0 ? "" : ", ", rand.Next(min, max + 1));
            }


            Console.WriteLine("\n\nplease enter any key to exit");
            Console.ReadKey();

        }
    }
}
