﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleListInFile
{
    class Person
    {
        string _name;
        int _age;
        string _city;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Name must be between 2 and 100 characters");
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must between 0 and 150");
                }
                _age = value;
            }
        }


        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("City must be between 2 and 100 characters");
                }
                _city = value;
            }
        }
        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }

        override
        public string ToString()
        {
            return String.Format("{0} is {1} from {2}", Name, Age, City);
        }
    }
    class Program
    {
        static List<Person> people = new List<Person>();

        const string FPath = @"../../people1.txt";
        static void ReadAllPeopleFromFile()
        {

            try
            {
                string[] lines = File.ReadAllLines(FPath);
                if (lines.Length < 1)
                {
                    throw new IOException("File is empty!");
                }

                for (int i = 0; i < lines.Length; i++)
                {
                    try
                    {
                        string[] tokens = lines[i].Split(';');
                        int age;
                        if (!int.TryParse(tokens[1], out age))
                        {
                            throw new ArgumentException(String.Format("invalid number for age, the line is ignored!", i + 1));
                        }

                        if (tokens.Length != 3)
                        {
                            Console.WriteLine("File line {0}: wrong amount of data, the line is ignored!", i + 1);
                        }
                        else
                        {
                            people.Add(new Person(tokens[0], age, tokens[2]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("File line " + (i + 1) + ": " + ex.GetType() + ": " + ex.Message);
                    }

                }

            }
            catch (Exception ex)
            {
                if (ex is FileNotFoundException)
                {
                    Console.WriteLine("File not found!");
                }
                else if (ex is IOException)
                {
                    Console.WriteLine("{0}: There is something wrong with the file ({1})!", ex.GetType(), ex.Message);
                }

            }
            finally
            {
                Console.Write("Press any key to continue!\n");
                Console.ReadKey();
            }
        }
        static void AddPersonInfo()
        {
            string name, city;
            int age;

            while (true)
            {
                Console.Write("Enter name: ");
                name = Console.ReadLine().Trim();
                if (name == "")
                {
                    Console.WriteLine("Name cannot be empty, please try again");
                }
                else { break; }
            }
            while (true)
            {
                Console.Write("Enter age:");
                if (!int.TryParse(Console.ReadLine(), out age))
                {
                    Console.WriteLine("Invalid number for age, try again!");
                }
                else { break; }
            }
            while (true)
            {
                Console.Write("Enter city: ");
                city = Console.ReadLine().Trim();
                if (city == "")
                {
                    Console.WriteLine("City cannot be empty, please try again!");
                }
                else
                {
                    break;
                }
            }
            try
            {
                people.Add(new Person(name, age, city));
                // * AppendAllLines() can be used
                File.AppendAllText(FPath, "\n" + name + ";");
                File.AppendAllText(FPath, age + ";");
                File.AppendAllText(FPath, city);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        static void ListAllPersonsInfo()
        {
            people.ForEach(Console.WriteLine);
        }
        static void FindPersonByName()
        {
            string query;
            while (true)
            {
                Console.WriteLine("Enter partial person name:");
                query = Console.ReadLine();
                if (query == "")
                {
                    Console.WriteLine("Empty partial name, try again!");
                }
                else { break; }
            }
            List<Person> pFound = new List<Person>();
            people.ForEach(obj =>
            {
                if (obj.Name.Contains(query))
                {
                    pFound.Add(obj);
                }
            });

            if (pFound.Count > 0)
            {
                Console.WriteLine("Matches found:");
                pFound.ForEach(Console.WriteLine);
            }
            else { Console.WriteLine("Match not found"); }
        }
        static void FindPersonYoungerThan()
        {
            int query;
            while (true)
            {
                Console.WriteLine("Enter maximum age:");
                if (!int.TryParse(Console.ReadLine(), out query))
                {
                    Console.WriteLine("invalid age, please enter again");
                    continue;
                }
                else if (query < 0)
                {
                    Console.WriteLine("age cannot be negative, try again!");
                    continue;
                }
                else { break; }
            }
            List<Person> pFound = new List<Person>();
            people.ForEach(obj =>
            {
                if (obj.Age < query)
                {
                    pFound.Add(obj);
                }
            });

            if (pFound.Count > 0)
            {
                Console.WriteLine("Matches found:");
                pFound.ForEach(Console.WriteLine);
            }
            else { Console.WriteLine("Match not found"); }
        }


        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            while (true)
            {
                Console.Write("\n" +
                "What do you want to do\n" +
                "1. Add person info\n" +
                "2. List persons info\n" +
                "3. Find a person by name\n" +
                "4. Find all persons younger than age\n" +
                "0. Exit\n" +
                "Choice: "
                );

                string str = Console.ReadLine();
                switch (str)
                {
                    case "1":
                        {
                            AddPersonInfo();
                            break;
                        }
                    case "2":
                        {
                            ListAllPersonsInfo();
                            break;
                        }
                    case "3":
                        {
                            FindPersonByName();
                            break;
                        }
                    case "4":
                        {
                            FindPersonYoungerThan();
                            break;
                        }
                    case "0":
                        {
                            Console.WriteLine("Good Bye\nPress any key to exist");
                            Console.ReadKey();
                            Environment.Exit(0);
                            break;
                        }
                    default:
                        Console.WriteLine("Invalid choice, try again.");
                        break;
                }
            }
        }
    }
}