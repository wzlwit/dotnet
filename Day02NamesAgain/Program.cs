﻿using System;

namespace Day02NamesAgain
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] nameList = new string[5];
            for (int i = 0; i < 5; i++)
            {
                while (true)
                {
                    Console.Write("Please enter name {0}: ", i + 1);
                    string input = Console.ReadLine();
                    if (String.Compare(input.Trim(), "") != 0)
                    {
                        nameList[i] = input;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("the name is empty, please try again!");
                    }
                }
            }

            Console.Write("\nPlease enter a serach string: ");

            string search = Console.ReadLine();

            Array.ForEach(nameList, name =>
            {
                if (name.Contains(search))
                {
                    Console.WriteLine("Matching name: {0}", name);
                }
            });

            string longest = nameList[0];
            for (int i = 0; i < nameList.Length; i++)
            {
                if (nameList[i].Length > longest.Length)
                {
                    longest = nameList[i];
                }
            }
            Console.WriteLine("\nLongest name is: " + longest);

            Console.ReadKey();
        }

    }
}
