﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Reset();
        }
        private void Reset (){
            tb1.IsEnabled = true;
            tb2.IsEnabled = false;
            btEqual.IsEnabled = true;
            }

        private void Bt_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtCE_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtEqual_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
