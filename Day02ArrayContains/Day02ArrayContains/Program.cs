﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02ArrayContains
{
    class Program
    {
        // * part 1, concatenate two arrays
        public int[] Concatenate(int[] a1, int[] a2)
        {
            int[] output = new int[a1.Length + a2.Length];
            for (int i = 0; i < a1.Length; i++)
            {
                output[i] = a1[i];
            }
            for (int i = a1.Length; i < a1.Length + a2.Length; i++)
            {
                output[i] = a2[i - a1.Length];
            }
            return output;
        }

        // part 2 print unique values
        public static void PrintDups(int[] a1, int[] a2)
        {
            for (int i = 0; i < a1.Length; i++)
            {
                bool eq = false;
                for (int j = 0; j < a2.Length; j++)
                {
                    if (a1[i] == a2[j])
                    {
                        Console.WriteLine(a1[i]);
                    }
                }

            }
        }

        // using ForEach to print duplicates in 1d array
        public static void PrintDupsFE(int[] a1, int[] a2)
        {

            Array.ForEach<int>(a1, e1 =>
            {
                Array.ForEach<int>(a2, e2 =>
                {
                    if (e1 == e2)
                    {
                        Console.WriteLine(e1);
                    }
                });
            });
        }


        // * part 3, remove duplications
        public int[] RemoveDups(int[] a1, int[] a2)
        {
            List<int> temp = new List<int>();

            // get unique value from array1:
            for (int i = 0; i < a1.Length; i++)
            {
                bool eq = false;
                for (int j = 0; j < a2.Length; j++)
                {
                    if (a1[i] == a2[j])
                    {
                        eq = true;
                    }
                }
                if (!eq)
                {
                    temp.Add(a1[i]);
                }
            }

            // get unique value from ary2:
            for (int i = 0; i < a2.Length; i++)
            {
                bool eq = false;
                for (int j = 0; j < a1.Length; j++)
                {
                    if (a2[i] == a1[j])
                    {
                        eq = true;
                    }
                }
                if (!eq)
                {
                    temp.Add(a2[i]);
                }
            }

            int[] outAry = new int[temp.Count];
            for (int i = 0; i < outAry.Length; i++)
            {
                outAry[i] = temp[i];
            }
            return outAry;
        }

        // * part 4, print duplications for 2D array 

        // using loop:
        public static void PrintDups(int[,] a1, int[,] a2)
        {
            for (int i = 0; i < a1.GetLength(0); i++)
            {
                for (int j = 0; j < a2.GetLength(1); j++)
                {
                    for (int m = 0; m < a2.GetLength(0); m++)
                    {
                        for (int n = 0; n < a2.GetLength(1); n++)
                        {
                            if (a1[i, j] == a2[m, n])
                            {
                                Console.WriteLine(a1[i, j]);
                            }
                        }
                    }
                }
            }
        }

        // using ForEach to print duplicates in 2d array
        public static void PrintDupsFE(int[,] a1, int[,] a2)
        {
            foreach (int e1 in a1)
            {
                foreach (int e2 in a2)
                {
                    if (e1 == e2)
                    {
                        Console.WriteLine(e1);
                    }
                }
            }
        }



        static void Main(string[] args)
        {
            Program testObj = new Program();
            int[] ary1 = { 2, 7, 8 };
            int[] ary2 = { -2, 9 };
            int[] myEntry = testObj.Concatenate(ary1, ary2);

            Console.WriteLine("\nfull array is: ");
            Console.WriteLine(string.Join(", ", myEntry));

            //  part 2
            ary1 = new int[] { 1, 3, 7, 8, 2, 7, 9, 11 };
            ary2 = new int[] { 3, 8, 7, 5, 13, 5, 12 };
            Console.WriteLine("the dupplications are:");
            PrintDups(ary1, ary2);

            // part3
            ary1 = new int[] { 2, 3, 7, 3 };
            ary2 = new int[] { 3, 9, 7 };
            Console.WriteLine("\nunique values in the two arrays are:");
            Console.WriteLine(string.Join("\n", testObj.RemoveDups(ary1, ary2)));

            // part4
            int[,] ary3 = { { 1, 3, 7, 8, 2, 7, 9 }, { 3, 8, 7, 5, 13, 5, 12 } };
            int[,] ary4 = { { 2, 3, 7, }, { 3, 9, 7 } };
            Console.WriteLine("\nduplicates values in 2D arrays are:");
            PrintDups(ary3, ary4);

            Console.WriteLine("\nduplicates values in arrays are (uising foreach):");
            PrintDupsFE(ary1, ary2);

            Console.WriteLine("\nduplicates values in arrays are (uising foreach):");
            PrintDupsFE(ary3, ary4);

            Console.ReadKey();
        }
    }
}
