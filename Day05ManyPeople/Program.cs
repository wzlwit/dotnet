﻿using Day05ManyPeople;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05ManyPeople
{
    class Person
    {
        string _name;
        int _age;

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        // * these two are equivalent
        public double weight { get; set; }
        private double _weight2;
        public double weight2
        {
            get { return _weight2; }
            set { _weight2 = value; }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must between 0 and 150");
                }
                _age = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}: {1} is {2} y/o", this.GetType().Name, Name, Age);
        }
    }

    class Teacher : Person
    {
        string _field;
        int _YOE;
        // public Teacher(string name, int age, string field, int yoe)
        public Teacher(string name, int age, string field, int yoe) : base(name, age)
        {
            // Name = name;
            // Age = age;
            Field = field;
            YOE = yoe;
        }
        public string Field
        {
            get { return _field; }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Field must be at least 2 characters long");
                }
                _field = value;
            }
        }
        public int YOE
        {
            get { return _YOE; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException("Year of experience must between 0 and 100");
                }
                _YOE = value;
            }
        }
        public override string ToString()
        {
            return String.Format("{0}: {1} is {2} y/o, with {3} years experience in {4}", this.GetType().Name, Name, Age, YOE, Field);
        }
    }

    class Student : Person
    {
        string _program;
        double _GPA;
        // public Student(string name, int age, string field, int yoe) //! base must be used
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            // Name = name; //! this can work, but duplicated and/or conflict with base
            // Age = age;
            Program = program;
            GPA = gpa;
        }
        public string Program
        {
            get { return _program; }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Program must be at least 2 characters long");
                }
                _program = value;
            }
        }
        public double GPA
        {
            get { return _GPA; }
            set
            {
                if (value < 0 || value > 5)
                {
                    throw new ArgumentOutOfRangeException("GPA must between 0 and 5");
                }
                _GPA = value;
            }
        }
        public override string ToString()
        {
            return String.Format("Student: {0} is {1} y/o, with GPA {2}  in {3}", Name, Age, GPA, Program);
        }
    }

    class Program
    {
        static List<Person> people = new List<Person>();

        //helper method to compare if the string is one of the names of the classes above
        static bool equalPerson(string type)
        {
            return (type.Equals("Person", StringComparison.OrdinalIgnoreCase));
        }
        static bool equalTeacher(string type)
        {
            return (type.Equals("Teacher", StringComparison.OrdinalIgnoreCase));
        }
        static bool equalStudent(string type)
        {
            return (type.Equals("Student", StringComparison.OrdinalIgnoreCase));
        }

        const string FFPath = @"..\..\people.txt";
        //helper method to read all data from file
        static void ReadAllPeople()
        {
            try
            {
                string[] lines = File.ReadAllLines(FFPath);


                if (lines.Length <= 0)
                {
                    Console.WriteLine("Empty file!");
                }
                else
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        string type = "", name = "", field = "", program = "";
                        int age = -1, yoe = -1;
                        double gpa = -1;

                        string[] line = lines[i].Split(';');

                        type = line[0];

                        if (equalPerson(type))
                        {
                            if (line.Length != 3)
                            {
                                Console.WriteLine("Line {0}: Incorrect number of fields. Ignored!", i + 1);

                                continue;
                            }
                            else
                            {
                                name = line[1].Trim();
                                if (name == "")
                                {
                                    Console.WriteLine("Line {0}: name is empty. Ignored!", i + 1);
                                    continue;
                                }
                                if (!int.TryParse(line[2].Trim(), out age))
                                {
                                    Console.WriteLine("Line {0}: Invalid age. Ignored!", i + 1);
                                    continue;
                                }

                                try
                                {

                                    people.Add(new Person(name, age));
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Line {0}: {1}, {2}. Ignored!", i + 1, ex.GetType(), ex.Message);
                                    continue;
                                }
                            }
                        }
                        else if (equalTeacher(type))
                        {
                            if (line.Length == 5)
                            {
                                name = line[1].Trim();
                                if (name == "")
                                {
                                    Console.WriteLine("Line {0}: name is empty. Ignored!", i + 1);
                                    continue;
                                }
                                if (!int.TryParse(line[2].Trim(), out age))
                                {
                                    Console.WriteLine("Line {0}: Invalid age. Ignored!", i + 1);
                                    continue;
                                }
                                field = line[3].Trim();
                                if (field == "")
                                {
                                    Console.WriteLine("Line {0}: Field is empty. Ignored!", i + 1);
                                    continue;
                                }
                                if (!int.TryParse(line[4].Trim(), out yoe))
                                {
                                    Console.WriteLine("Line {0}: Invalid year of experience. Ignored!", i + 1);
                                    continue;
                                }

                                try
                                {

                                    people.Add(new Teacher(name, age, field, yoe));
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Line {0}: {1}, {2}. Ignored!", i + 1, ex.GetType(), ex.Message);
                                    continue;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Line {0}: incorrect number of data field. Ignored!", i + 1);
                                continue;
                            }
                        }
                        else if (equalStudent(type))
                        {
                            if (line.Length == 5)
                            {
                                name = line[1].Trim();
                                if (name == "")
                                {
                                    Console.WriteLine("Line {0}: name is empty. Ignored!", i + 1);
                                    continue;
                                }
                                if (!int.TryParse(line[2].Trim(), out age))
                                {
                                    Console.WriteLine("Line {0}: Invalid age. Ignored!", i + 1);
                                    continue;
                                }
                                program = line[3].Trim();
                                if (program == "")
                                {
                                    Console.WriteLine("Line {0}: programe is empty. Ignored!", i + 1);
                                    continue;
                                }
                                if (!double.TryParse(line[4].Trim(), out gpa))
                                {
                                    Console.WriteLine("Line {0}: Invalid year of experience. Ignored!", i + 1);
                                    continue;
                                }

                                try
                                {
                                    people.Add(new Student(name, age, program, gpa));
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Line {0}: {1}, {2}. Ignored!", i + 1, ex.GetType(), ex.Message);
                                    continue;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Line {0}: incorrect number of data field. Ignored!", i + 1);
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Line {0}: wrong type. Ignored!");
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("I/O exception, no data read from file!");
            }
        }

        static void ListPerson()
        {
            people.ForEach(e =>
            {
                if (equalPerson(e.GetType().Name))
                {
                    Console.WriteLine(e);
                }
            });
        }
        static void ListTeacher()
        {
            people.ForEach(e =>
            {
                if (equalTeacher(e.GetType().Name))
                {
                    Console.WriteLine(e);
                }
            });
        }
        static void ListStudent()
        {
            people.ForEach(e =>
            {
                if (equalStudent(e.GetType().Name))
                {
                    Console.WriteLine(e);
                }
            });
        }

        static void Main(string[] args)
        {
            try
            {


                ReadAllPeople();
                Console.WriteLine();
                people.ForEach(Console.WriteLine);
                Console.WriteLine("\n\nList them in groups:\n");
                ListPerson();
                Console.WriteLine();
                ListTeacher();
                Console.WriteLine();
                ListStudent();


            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}: {1}", ex.GetType(), ex.Message);
            }
            finally
            {
                Console.WriteLine("\n\nPress any key to exist");
                Console.ReadLine();
                Environment.Exit(1);
            }

        }
    }
}

