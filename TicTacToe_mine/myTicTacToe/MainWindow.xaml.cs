﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace myTicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            buttons[0, 0] = bt00;
            buttons[0, 1] = bt01;
            buttons[0, 2] = bt02;

            buttons[1, 0] = bt10;
            buttons[1, 1] = bt11;
            buttons[1, 2] = bt12;

            buttons[2, 0] = bt20;
            buttons[2, 1] = bt21;
            buttons[2, 2] = bt22;

            foreach (Button btn in buttons)
            {
                btn.IsEnabled = false;
                btn.Content = "";
                btn.FontSize = 64;
                btn.VerticalContentAlignment = VerticalAlignment.Center;
            }

            btGame.IsEnabled = false;

        }

        bool player1 = true;

        Button[,] buttons = new Button[3, 3];
        private void winCheck()
        {
            if (
                buttons[0, 0].Content.ToString() != "" && buttons[0, 0].Content.ToString() == buttons[0, 1].Content.ToString() && buttons[0, 1].Content.ToString() == buttons[0, 2].Content.ToString() ||
                buttons[1, 0].Content.ToString() != "" && buttons[1, 0].Content.ToString() == buttons[1, 1].Content.ToString() && buttons[1, 1].Content.ToString() == buttons[1, 2].Content.ToString() ||
                buttons[2, 0].Content.ToString() != "" && buttons[2, 0].Content.ToString() == buttons[2, 1].Content.ToString() && buttons[2, 1].Content.ToString() == buttons[2, 2].Content.ToString() ||

                buttons[0, 0].Content.ToString() != "" && buttons[0, 0].Content.ToString() == buttons[1, 0].Content.ToString() && buttons[1, 0].Content.ToString() == buttons[2, 0].Content.ToString() ||
                buttons[0, 1].Content.ToString() != "" && buttons[0, 1].Content.ToString() == buttons[1, 1].Content.ToString() && buttons[1, 1].Content.ToString() == buttons[2, 1].Content.ToString() ||
                buttons[0, 2].Content.ToString() != "" && buttons[0, 2].Content.ToString() == buttons[1, 2].Content.ToString() && buttons[1, 2].Content.ToString() == buttons[2, 2].Content.ToString() ||

                buttons[0, 0].Content.ToString() != "" && buttons[0, 0].Content.ToString() == buttons[1, 1].Content.ToString() && buttons[1, 1].Content.ToString() == buttons[2, 2].Content.ToString() ||
                buttons[2, 0].Content.ToString() != "" && buttons[2, 0].Content.ToString() == buttons[1, 1].Content.ToString() && buttons[1, 1].Content.ToString() == buttons[0, 2].Content.ToString()
                )
            {
                string winner = player1 ? "Player O" : "Player X";
                MessageBox.Show(string.Format("Winner: " + winner));
            }
        }


        private void BtGame_Click(object sender, RoutedEventArgs e)
        {
            if (btGame.Content.ToString() == "Start")
            {
                foreach (Button btn in buttons)
                {
                    btn.IsEnabled = true;
                    btn.Content = "";
                }
                tbPlayerO.IsEnabled = false;
                tbPlayerX.IsEnabled = false;
                lbO.Background = Brushes.Red;
                btGame.Content = "Stop";
            }
            else if (btGame.Content.ToString() == "Stop")
            {
                foreach (Button btn in buttons)
                {
                    btn.IsEnabled = false;
                }
                tbPlayerO.IsEnabled = true;
                tbPlayerX.IsEnabled = true;
                btGame.Content = "Start";
            }
            else
            {
                MessageBox.Show("unknow error");
            }
        }

        private void TbPlayer_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbPlayerO.Text.Trim() != "" && tbPlayerX.Text.Trim() != "")
            {
                btGame.IsEnabled = true;
            }
            else
            {
                btGame.IsEnabled = false;
            }
        }

        private void Bt_Click(object sender, RoutedEventArgs e)
        {
            if (player1)
            {
                Button clicked = (Button)sender;
                clicked.Content = "O";
                
                clicked.IsEnabled = false;
                winCheck();
                lbO.Background = Brushes.White ;
                lbX.Background = Brushes.Blue;
                player1 = !player1;
                
            }
            else
            {
                Button clicked = (Button)sender;
                clicked.Content = "X";
                winCheck();
                lbX.Background = Brushes.White;
                lbO.Background = Brushes.Red;
                clicked.IsEnabled = false;
                player1 = !player1;
            }
        }
    }
}
