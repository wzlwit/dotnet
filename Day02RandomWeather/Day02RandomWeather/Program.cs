﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02RandomWeather
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Random rand = new Random();
                int temp = rand.Next(-30, 31);
                Console.WriteLine("\nThe temperature is: " + temp);
                if (temp <= -15)
                {
                    Console.WriteLine("Very very cold!");
                }
                else if (temp < 0)
                {
                    Console.WriteLine("Freezing already!");
                }
                else if (temp <= 15)
                {
                    Console.WriteLine("Spring of Fall!");
                }
                else
                {
                    Console.WriteLine("That is what I like!");
                }
                if (temp == 30)
                {
                    Console.WriteLine("Hottes 31 ! Enter any key to exit");
                    break;
                }
                Console.ReadKey();
            }
        }
    }
}
