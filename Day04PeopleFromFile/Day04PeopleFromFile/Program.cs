﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleFromFile
{
    class Person
    {
        string _name;
        int _age;

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        // * these two are equivalent
        public double weight { get; set; }
        private double _weight2;
        public double weight2
        {
            get { return _weight2; }
            set { _weight2 = value; }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must between 0 and 150");
                }
                _age = value;
            }


        }
        override
        public string ToString()
        {
            return String.Format("{0} is {1} y/o", Name, Age);
        }
    }

    class Program
    {
        const string PATH = @"..\..\..\people.txt";
        // const string PATH = "../../../people.txt"; //using '/' is ok 
        static List<Person> people = new List<Person>();

        static void Main(string[] args)
        {
            string[] output = File.ReadAllLines(PATH);
            foreach (string str in output)
            {
                string[] strSplit = str.Split(';');
                // Console.Write(strSplit[0]+strSplit[1]+"\n");
                people.Add(new Person(strSplit[0], int.Parse(strSplit[1])));
            }
            foreach (Person person in people)
            {
                Console.WriteLine(person);
            }

            Console.ReadKey();
        }


    }
}
