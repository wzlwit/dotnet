﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05Virtuals
{
    class A
    {
        public void Foo() { Console.WriteLine("A::Foo()"); }
    }
    class B : A
    {
        public void Foo() { Console.WriteLine("b::foo()"); }
    }
    class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            B b = new B();

            b.Foo();
            Console.ReadKey();
        }
    }
}
