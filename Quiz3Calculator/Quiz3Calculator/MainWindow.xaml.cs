﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string op; //to store operator
        TextBox tbCurrent;
        public MainWindow()
        {
            InitializeComponent();
            Reset();
            tbCurrent = tb1;
            tbResult.IsEnabled = false;
        }

        private void Reset()
        {
            op = null;
            tb1.IsEnabled = true;
            tb2.IsEnabled = false;
            tbCurrent = tb1;
            tbCurrent.Focus();
            SetNumBtn(true);
            SetOpBtn(true);
            btSign.IsEnabled = true;

            btEqual.IsEnabled = false;
            tb1.Text = "";
            tb2.Text = "";
            tbResult.Text = "";
            tbResult.Background = null;
            lbSign.Content = "";
        }
        string preVal; //for origin /new value

        private bool NumberCheck(string valStr)
        {
            //const string notneg = @"^[-]?[0-9]*.?[0-9]$";
            //if (val.Trim() != "" && !Regex.IsMatch(val, notneg))
            //if (!Regex.IsMatch(val, notneg))
            double val;
            if (valStr.Trim() != "")
            {
                if (!Double.TryParse(valStr, out val))
                {
                    MessageBox.Show("Invalid number");
                    return false;
                }
            }
            return true;

        }

        private void SetNumBtn(bool enable)
        {
            btn0.IsEnabled = enable;
            btn1.IsEnabled = enable;
            btn2.IsEnabled = enable;
            btn3.IsEnabled = enable;
            btn4.IsEnabled = enable;
            btn5.IsEnabled = enable;
            btn6.IsEnabled = enable;
            btn7.IsEnabled = enable;
            btn8.IsEnabled = enable;
            btn9.IsEnabled = enable;
            btnDot.IsEnabled = enable;

        }
        private void SetOpBtn(bool enable)
        {

            btM.IsEnabled = enable;
            btP.IsEnabled = enable;
            btD.IsEnabled = enable;
            bt_.IsEnabled = enable;
        }

        private void btOp_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            lbSign.Content = btn.Content;
            op = btn.Content.ToString();

            tb1.IsEnabled = false;
            tb2.IsEnabled = true;
            tbCurrent = tb2;
            tbCurrent.Focus();
            //btEqual.IsEnabled = false;

        }

        private void Bt_Click(object sender, RoutedEventArgs e)
        {

            Button cur = (Button)sender;
            preVal = tbCurrent.Text;
            if (NumberCheck(preVal + cur.Content.ToString()))
            {
                tbCurrent.Text = preVal + cur.Content.ToString();
                if (tbCurrent == tb2)
                {
                    btEqual.IsEnabled = true;
                }
                tbCurrent.Focus();
                tbCurrent.SelectionStart = tbCurrent.Text.Length;
            }
        }

        private void BtEqual_Click(object sender, RoutedEventArgs e)
        {
            double? res = null;

            switch (op)
            {
                case "+":
                    {
                        res = double.Parse(tb1.Text) + double.Parse(tb2.Text);
                        break;
                    }

                case "-":
                    {
                        res = double.Parse(tb1.Text) - double.Parse(tb2.Text);
                        break;
                    }

                case "*":
                    {
                        res = double.Parse(tb1.Text) * double.Parse(tb2.Text);
                        break;
                    }

                case "/":
                    {
                        double num = double.Parse(tb2.Text);
                        if (num == 0)
                        {
                            MessageBox.Show(" cannot be divided by 0"); Reset(); break;
                        }
                        res = double.Parse(tb1.Text) / num;
                        break;
                    }
                default: MessageBox.Show("unknow operation!"); Reset(); break;

            }
            tb2.IsEnabled = false;
            SetNumBtn(false);
            SetOpBtn(false);
            btSign.IsEnabled = false;

            tbCurrent = null;
            tbResult.Text = res.ToString();
            tbResult.Background = Brushes.Yellow;
        }

        private void BtCE_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }


        private void Tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox cur = (TextBox)sender;
            NumberCheck(cur.Text);
        }

        private void BtSign_Click(object sender, RoutedEventArgs e)
        {
            Button op = (Button)sender;
            if (tbCurrent.Text.Trim() == "")
            {
                MessageBox.Show("Empty Number!  ");
            }
            else
            {
                if (tbCurrent.Text[0] == '-')
                {
                    tbCurrent.Text = tbCurrent.Text.Substring(1);

                }
                else
                {
                    tbCurrent.Text = '-' + tbCurrent.Text;
                }
            }
        }

        private void Tb1_GotFocus(object sender, RoutedEventArgs e)
        {
            tbCurrent = tb1;
        }

        private void Tb2_GotFocus(object sender, RoutedEventArgs e)
        {
            tbCurrent = tb2;
        }
    }
}
