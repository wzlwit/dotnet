﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Triple
{
    class Program
    {
        static List<double> gpa = new List<double>();
        static List<string> grades = new List<string>();
        static double sum = 0;
        static double fib = 0;

        // part 1 methods
        static void GPACalculator()
        {
            sum = gpa[0];
            for (int i = 1; i < gpa.Count; i++)
            {
                sum += gpa[i];
            }
            sum = sum / gpa.Count;
        }

        // helper method
        static double LetterGradetoNumber(String grade)
        {
            switch (grade)
            {
                case "A": return 4;
                case "A-": return 3.7;
                case "B+": return 3.3;
                case "B": return 3;
                case "B-": return 2.7;
                case "C+": return 2.3;
                case "C": return 2;
                case "D": return 1;
                case "F": return 0;
                default: return -1;
            }
        }

        // part 2 methods;
        static bool isPerfectSquare(double x)
        {
            int s = (int)Math.Sqrt(x);
            return (s * s == x);
        }
        static bool FibChecker(double n)
        {
            // n is Fibonacci if one of 
            // 5*n*n + 4 or 5*n*n - 4 or  
            // both are a perfect square 
            return isPerfectSquare(5 * n * n + 4) ||
                   isPerfectSquare(5 * n * n - 4);
        }


        // * a valide line of matrix entry
        // validation for entry, pulled out as a method
        static void MatrixEntry(double[,] mtx)
        {
            string[] buf;
            for (int i = 0; i < mtx.GetLength(0); i++)
            {

                while (true)
                {
                    bool shouldBreak = false;
                    // Console.WriteLine("The first line:");
                    buf = Console.ReadLine().Split(',');
                    if (buf.Length != mtx.GetLength(1))
                    {
                        Console.WriteLine("Incorrect amount of numbers!\n please try again");
                    }
                    else
                    {
                        for (int j = 0; j < mtx.GetLength(1); j++)
                        {
                            if (!double.TryParse(buf[j], out mtx[i, j]))
                            {
                                Console.WriteLine("element must be a number, Try for this line again!");
                                shouldBreak = false;
                                break;
                            }
                            shouldBreak = true;
                        }
                        if (shouldBreak)
                        {
                            break;
                        }
                    }
                }
            }
        }
        // print out matrix
        static void PrMatrix(double[,] mtx)
        {
            Console.WriteLine("\nThe matrix is:");
            for (int i = 0; i < mtx.GetLength(0); i++)
            {
                for (int j = 0; j < mtx.GetLength(1); j++)
                {
                    Console.Write((j == 0 ? "" : ", ") + mtx[i, j]);
                }
                Console.WriteLine();
            }
        }
        static double[,] MatrixMultiplier(double[,] Ma, double[,] Mb)
        {
            int D = Ma.GetLength(0);
            double[,] result = new double[D, D];
            if (D == Mb.GetLength(1))
            {
                for (int i = 0; i < D; i++)
                {
                    for (int j = 0; j < D; j++)
                    {
                        for (int m = 0; m < Ma.GetLength(1); m++)
                        {
                            result[i, j] += Ma[i, m] * Mb[m, j];
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("the two matrix cannot multiply");
            }
            return result;
        }
        static void Main(string[] args)
        {
            // * quiz 1
            Console.WriteLine("Enter letter grades, one per line:");

            while (true)
            {
                Console.Write("Enter the grad (empty line and Enter to escape): ");
                string input = Console.ReadLine().Trim();
                if (input == "")
                {
                    break;
                }
                else
                {
                    double grd = LetterGradetoNumber(input);
                    if (grd == -1)
                    {
                        Console.WriteLine("invalide grade, try again!");
                    }
                    else
                    {

                        grades.Add(input);
                        gpa.Add(grd);
                    }
                }
            }
            Console.WriteLine("\nYou've entered:");
            for (int i = 0; i < grades.Count; i++)
            {
                Console.WriteLine("{0} which is {1}", grades[i], gpa[i]);
            }

            GPACalculator();
            Console.WriteLine("\nThe GPA average is: " + sum);

            // * quiz 2:
            Console.WriteLine("\nQuiz2:");

            double fib = 0;
            Console.Write("Enter a number to check against Fibonacci:");
            if (!double.TryParse(Console.ReadLine(), out fib))
            {
                Console.WriteLine("Invalid number! Press any key to terminate.");
                Console.ReadKey();
                Environment.Exit(1);
            }
            if (fib < 0)
            {
                Console.WriteLine("The number must be not negative! Press any key to terminate.");
                Console.ReadKey();
                Environment.Exit(1);
            }
            if (FibChecker(fib))
            {
                Console.WriteLine(fib + " is a Fibonacci number");
            }
            else
            {
                Console.WriteLine(fib + " is not a Fibonacci number");
                while (true)
                {
                    if (FibChecker(++fib))
                    {
                        Console.WriteLine("the next Fibonacci number is " + fib);
                        break;
                    }
                }
            }

            // part 3
            double[,] matA = new double[2, 3];
            double[,] matB = new double[3, 2];

            Console.WriteLine("\nEnter the first matrix 2 lines of 3 numbers, comma-separated, line by line:");

            MatrixEntry(matA);
            PrMatrix(matA);

            Console.WriteLine("\nEnter the second matrix 3 lines of 2 numbers, comma-separated, line by line:");
            MatrixEntry(matB);
            PrMatrix(matB);
            double[,] result = MatrixMultiplier(matA, matB);
            
            PrMatrix(result);
            Console.WriteLine("(result)");
            Console.ReadKey();
        }
    }
}

