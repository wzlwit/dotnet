﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public static int GetAge(string msg)
        {
            bool intCheck = true;
            int outPut = 0;
            while (intCheck)
            {
                Console.Write(msg);
                intCheck = !Int32.TryParse(Console.ReadLine(), out outPut);
                if (intCheck)
                {
                    Console.WriteLine("It is not a valid number, please try again: ");
                }
                else if (outPut <= 0)
                {
                    Console.WriteLine("Age must be higher than 0");
                    intCheck = true;
                }
            }
            return outPut;
        }


        static void Main(string[] args)
        {
            Console.Write("Enter your name: ");

            string name = Console.ReadLine();

            int age = GetAge("Enter your age: ");

            Console.WriteLine("Hi {0}, you are {1} y/o.\nYou are {2}an adult{3}.", name, age, age>=18? "":"not ", age>=18?"":" yet");


            Console.WriteLine("\n\nEnter any key to exit");
            Console.ReadKey();
        }
    }
}
