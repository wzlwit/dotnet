﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Find
{
    class Program
    {

        // * enter five floating values
        public static List<double> EnterValues()
        {
            bool err;
            List<double> myNum = new List<double>();
            Console.WriteLine("Please enter five floating numbers:");
            for (int i = 0; i < 5; i++)
            {
                err = true;
                do
                {
                    Console.Write("Enter float number {0}: ", i + 1);
                    double outPut;
                    if (Double.TryParse(Console.ReadLine(), out outPut))
                    {
                        err = false;
                        myNum.Add(outPut);
                        // ! or use 'break' to terminate the while loop
                    }
                    else
                    {
                        Console.WriteLine("It's not a valid floating number, try again!");
                    }
                }
                while (err);
            }

            return myNum;
        }

        // * the smallest of the numbers
        public static double FindSmallestNumber(List<double> ary)
        {
            double smallest = ary[0];
            for (int i = 1; i < ary.Count; i++)
            {
                if (ary[i] < smallest)
                {
                    smallest = ary[i];
                }
            }
            return smallest;
        }

        // * the sum of all the numbers
        public static double FindSumOfAllNumbers(List<double> ary)
        {
            double sum = ary[0];
            for (int i = 1; i < ary.Count; i++)
            {
                sum += ary[i];
            }
            return sum;
        }

        // * the average of all those values
        public static double FindAverage(List<double> ary)
        {
            return FindSumOfAllNumbers(ary) / ary.Count;
        }

        // * standard deviation of those values
        public static double FindStandardDeviation(List<double> ary)
        {
            double sumSq = 0;
            double avg = FindAverage(ary);
            for (int i = 0; i < ary.Count; i++)
            {
                sumSq += Math.Pow((ary[i] - avg), 2);
            }
            return Math.Sqrt(sumSq / ary.Count);
            // return Math.Sqrt(sumSq / (ary.Count - 1)); //sample deviation
        }


        // * print out a List in one line
        public static void PrintList(List<double> ary)
        {
            for (int i = 0; i < ary.Count; i++)
            {
                Console.Write((i == 0 ? "" : ", ") + ary[i]);
            }
        }

        static void Main(string[] args)
        {
            List<double> myEntery = EnterValues();

            Console.WriteLine("\nThe five floating numbers are:");
            // user-definded method to print out a List
            PrintList(myEntery);

            Console.WriteLine("\n\nForEach() way to print a List:");
            // ! another tricky way print out a list
            myEntery.ForEach(Console.WriteLine);
            Console.WriteLine();

            // show the results
            Console.WriteLine("The statistics for the 5 values are:");
            Console.WriteLine("The smallest of the numbers is: " + FindSmallestNumber(myEntery));
            Console.WriteLine("The sum of all the numbers is: " + FindSumOfAllNumbers(myEntery));
            Console.WriteLine("The average of those values is: " + FindAverage(myEntery));
            Console.WriteLine("The stand deviation of those values is: " + FindStandardDeviation(myEntery));

            Console.WriteLine("\nenter any key to exit");
            Console.ReadKey();
        }
    }
}
