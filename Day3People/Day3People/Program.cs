﻿using Day3People;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3People
{
    class Person
    {
        string _name;
        int _age;

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        // * these two are equivalent
        public double weight { get; set; }
        private double _weight2;
        public double weight2
        {
            get { return _weight2; }
            set { _weight2 = value; }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must between 0 and 150");
                }
                _age = value;
            }
        }
    }

    class Teacher : Person
    {
        string _field;
        int _YOE;
        // public Teacher(string name, int age, string field, int yoe)
        public Teacher(string name, int age, string field, int yoe) : base(name, age)
        {
            // Name = name;
            // Age = age;
            Field = field;
            YOE = yoe;
        }
        public string Field
        {
            get { return _field; }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Field must be at least 2 characters long");
                }
                _field = value;
            }
        }
        public int YOE
        {
            get { return _YOE; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException("Year of experience must between 0 and 100");
                }
                _YOE = value;
            }
        }
    }

}

    class Student : Person
    {
        string _program;
        int _GPA;
        // public Student(string name, int age, string field, int yoe) //! base must be used
        public Student(string name, int age, string program, int gpa) : base(name, age)
        {
            // Name = name; //! this can work, but duplicated and/or conflict with base
            // Age = age;
            Program = program;
            GPA = gpa;
        }
        public string Program
        {
            get { return _program; }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Program must be at least 2 characters long");
                }
                _program = value;
            }
        }
        public int GPA
        {
            get { return _GPA; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException("GPA must between 0 and 90");
                }
                _GPA = value;
            }
        }
    }




class Program
{
    static void Main(string[] args)
    {
        try
        {
            // Person p = new Person() { Name = "Jerry", Age = 33 };//cannot for private
            // p.Name = "M"; //! this call the setter
            Person p = new Person("Jerr", 33);
            p.Age = -33;
            Console.WriteLine(p.Name + p.Age); //! this call the getter
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine("Exception: {0}", e.Message);
        }
        finally
        {
            Console.WriteLine("press any key to finish");
            Console.ReadKey();
        }

    }
}

