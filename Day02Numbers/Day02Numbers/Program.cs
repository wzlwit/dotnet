﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            while (true)
            {
                Console.Write("How many numbers to generate:");
                if (Int32.TryParse(Console.ReadLine(), out num) && num != 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("That is not a valid number, please try again!");
                }
            }

            Random rand = new Random();
            List<int> numList = new List<int>();
            for (int i = 0; i < num; i++)
            {
                numList.Add(rand.Next(-100, 101));
            }
            numList.ForEach((val) =>
            {
                if (val <= 0)
                    Console.WriteLine(val);
            });
            Console.ReadKey();
        }
    }
}
