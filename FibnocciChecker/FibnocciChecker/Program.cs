﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Triple
{
    class Program
    {
        static double sum = 0;
        static double fib = 0;
               
        // part 2 methods;
        static bool isPerfectSquare(double x)
        {
            int s = (int)Math.Sqrt(x);
            return (s * s == x);
        }
        static bool FibChecker(double n)
        {
            // n is Fibonacci if one of 
            // 5*n*n + 4 or 5*n*n - 4 or  
            // both are a perfect square 
            return isPerfectSquare(5 * n * n + 4) ||
                   isPerfectSquare(5 * n * n - 4);
        }


        static void Main(string[] args)
        {
            Console.WriteLine("\nQuiz2:");

            double fib = 0;
            Console.Write("Enter a number to check against Fibonacci:");
            if (!double.TryParse(Console.ReadLine(), out fib))
            {
                Console.WriteLine("Invalid number! Press any key to terminate.");
                Console.ReadKey();
                Environment.Exit(1);
            }
            if (fib < 0)
            {
                Console.WriteLine("The number must be not negative! Press any key to terminate.");
                Console.ReadKey();
                Environment.Exit(1);
            }
            if (FibChecker(fib))
            {
                Console.WriteLine(fib + " is a Fibonacci number");
            }
            else
            {
                Console.WriteLine(fib + " is not a Fibonacci number");
                while (true)
                {
                    if (FibChecker(++fib))
                    {
                        Console.WriteLine("the next Fibonacci number is " + fib);
                        break;
                    }
                }
            }

            Console.ReadKey();
        }
    }
}

