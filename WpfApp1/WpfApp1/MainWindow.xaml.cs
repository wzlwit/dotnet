﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        static bool Validation()
        {
            MainWindow cur = Application.Current.MainWindow as MainWindow;
            if (cur.tbName.Text.Trim() != "") {
                MessageBox.Show("Empty name");
                return false;
            };
            return false;
        }

        private void BtRegister_Click(object sender, RoutedEventArgs e)
        {
            Validation();
            MessageBox.Show(rbCat.IsChecked.GetValueOrDefault().ToString()); //or using (bool) to cast it into boolean

        }
    }
}
