﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07SayHello
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonHello_Click(object sender, RoutedEventArgs e)
        {
            string pattern = @"^[^\\]{2,30}$"; //with @, no need to put\\\\ like java. so without @ using java way for regex. with @ means normal way for regex
            string msg = "";
            int age;



            bool NameCheck = Regex.IsMatch(@tbName.Text, pattern); //!must put @ before Html Value to correctly compare!
            bool AgeCheck = int.TryParse(tbAge.Text, out age);
            if (AgeCheck)
            {
                if (age < 0 || age > 150)
                {
                    AgeCheck = false;
                }

            }

            if (NameCheck && AgeCheck)
            {
                string senderName = ((Button)sender).Name;
                switch (senderName)
                {
                    case "Hello":
                        {
                            MessageBox.Show(string.Format("Hello {0}, {1} years old!", tbName.Text, age));
                            break;
                        }
                    case "Save":
                        {
                            File.AppendAllText(@"..\..\people.txt", "\n" + tbName.Text + ";" + tbAge.Text);
                            MessageBox.Show("Successfuly saved1");
                            break;
                        }
                    default: MessageBox.Show("unknow error"); break;
                }
            }
            else
            {
                if (!NameCheck)
                {
                    msg = "Invalid name!";
                }
                if (!AgeCheck)
                {
                    msg += "Invalid age!";
                }
                MessageBox.Show(msg);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
