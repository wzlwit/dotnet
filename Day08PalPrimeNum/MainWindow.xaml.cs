﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08PalPrimeNum
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    // check number
    public partial class MainWindow : Window
    {
        //List<string> result = new List<string>();
        string fPath = @"PalPrime.txt";
        string result = "";

        private void CheckNumber(TextBox tb)
        {
            string inputStr = tb.Text;
            double input;
            if (inputStr.Trim() != "" && !double.TryParse(inputStr.Trim(), out input))
            {
                MessageBox.Show("Invalid number");
                if (inputStr.Length > 0)
                {
                    tb.Text = inputStr.Substring(0, inputStr.Length - 1);
                    tb.SelectionStart = inputStr.Length - 1;
                }
            }

        }

        //using regex to check numbers
        private void CheckNumberRegex(TextBox tb)
        {
            // non negative
            //const string notneg = @"^[0-9]*\.?[0-9]*$";

            //integer
            const string notneg = @"^[0-9]*$";

            string inputStr = tb.Text.Trim();
            if (tb.Text.Trim() != "" && !Regex.IsMatch(tb.Text, notneg))
            {
                MessageBox.Show("Invalid number");
                if (inputStr.Length > 0) //in case  out of inex exception
                {
                    tb.Text = inputStr.Substring(0, inputStr.Length - 1);
                    tb.SelectionStart = inputStr.Length - 1;
                }
            }

        }

        private bool IsPrime(int num) //if is a prime number
        {
            if (num <= 0) return false;
            for (int i = 2; i < num - 1; i++)
            {
                if (num % i == 0) { return false; }

            }
            return true;
        }

        private bool IsPalindrome(string msg) //string is palidrome or not
        {
            int len = msg.Length;
            for (int i = 0; i < len / 2; i++)
            {
                if (msg[i] != msg[len - 1 - i])
                {
                    return false;
                }
            }
            return true;
        }

        //generate numbers
        private void GenNum()
        {
            int start = int.Parse(tbStart.Text), end = int.Parse(tbEnd.Text);

            if (end <= start)
            {
                MessageBox.Show("Second number must be greater!");

                tbEnd.Focus();
                tbEnd.SelectAll();
            }
            else
            {
                string resultStr = "";
                if (chkbxPal.IsChecked == true)
                {
                    for (int i = start; i <= end; i++)
                    {
                        if (IsPalindrome(i.ToString()) && IsPrime(i))
                        {
                            resultStr += " " + i;
                        }
                    }
                    if (resultStr == "")
                    {
                        resultStr = " N/A";
                    }
                    resultStr = string.Format("Pal Prime numbers between {0} and {1} are:{2}\n", start, end, resultStr);
                    result += resultStr;
                }
                else
                {
                    for (int i = start; i <= end; i++)
                    {
                        if (IsPrime(i))
                        {
                            resultStr += " " + i;
                        }
                    }
                    if (resultStr == "")
                    {
                        resultStr = " N/A";
                    }
                    resultStr = string.Format("Prime numbers between {0} and {1} are:{2}\n", start, end, resultStr);
                    result += resultStr;
                }
                tblkShow.Text = result;
                svOutput.ScrollToBottom();
            }
        }


        public MainWindow()
        {
            InitializeComponent();
            tbStart.Focus();
        }

        private void TbStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            //CheckNumber(tbStart);
            CheckNumberRegex(tbStart);
        }

        private void TbEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            //tbEnd.KeyUp += TbEnd_KeyUp;
            //CheckNumber(tbEnd);
            CheckNumberRegex(tbEnd);
        }

        private void BtnGen_Click(object sender, RoutedEventArgs e)
        {
            GenNum();
        }

        private void MiClose_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(1);
        }

        private void MiExport_Click(object sender, RoutedEventArgs e)
        {
            File.AppendAllText(fPath, result);
            tblkShow.Text = "";
        }

        private void MiOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fOpen = new OpenFileDialog();
            if (fOpen.ShowDialog() == true)
            {
                if (fOpen.FileName != "")
                {
                    fPath = fOpen.FileName;
                    tblkShow.Text = File.ReadAllText(fPath);
                }
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            result = "";
            tblkShow.Text = "";
        }

        private void TbEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = true;
                GenNum();
            }
        }
    }
}
