﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Triple
{
    class Program
    {
        // * a valide line of matrix entry
        // validation for entry, pulled out as a method
        static void MatrixEntry(double[,] mtx)
        {
            string[] buf;
            for (int i = 0; i < mtx.GetLength(0); i++)
            {

                while (true)
                {
                    bool shouldBreak = false;
                    // Console.WriteLine("The first line:");
                    buf = Console.ReadLine().Split(',');
                    if (buf.Length != mtx.GetLength(1))
                    {
                        Console.WriteLine("Incorrect amount of numbers!\n please try again");
                    }
                    else
                    {
                        for (int j = 0; j < mtx.GetLength(1); j++)
                        {
                            if (!double.TryParse(buf[j], out mtx[i, j]))
                            {
                                Console.WriteLine("element must be a number, Try for this line again!");
                                shouldBreak = false;
                                break;
                            }
                            shouldBreak = true;
                        }
                        if (shouldBreak)
                        {
                            break;
                        }
                    }
                }
            }
        }
        // print out matrix
        static void PrMatrix(double[,] mtx)
        {
            Console.WriteLine("\nThe matrix is:");
            for (int i = 0; i < mtx.GetLength(0); i++)
            {
                for (int j = 0; j < mtx.GetLength(1); j++)
                {
                    Console.Write((j == 0 ? "" : ", ") + mtx[i, j]);
                }
                Console.WriteLine();
            }
        }
        static double[,] MatrixMultiplier(double[,] Ma, double[,] Mb)
        {
            int D = Ma.GetLength(0);
            double[,] result = new double[D, D];
            if (D == Mb.GetLength(1))
            {
                for (int i = 0; i < D; i++)
                {
                    for (int j = 0; j < D; j++)
                    {
                        for (int m = 0; m < Ma.GetLength(1); m++)
                        {
                            result[i, j] += Ma[i, m] * Mb[m, j];
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("the two matrix cannot multiply");
            }
            return result;
        }
        static void Main(string[] args)
        {

            double[,] matA = new double[2, 3];
            double[,] matB = new double[3, 2];

            Console.WriteLine("\nEnter the first matrix 2 lines of 3 numbers, comma-separated, line by line:");

            MatrixEntry(matA);
            PrMatrix(matA);

            Console.WriteLine("\nEnter the second matrix 3 lines of 2 numbers, comma-separated, line by line:");
            MatrixEntry(matB);
            PrMatrix(matB);
            double[,] result = MatrixMultiplier(matA, matB);

            PrMatrix(result);
            Console.WriteLine("(result)");
            Console.ReadKey();
        }
    }
}

