﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07TemConv
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MyConvert:Window
    {
        public static MainWindow cur = new MainWindow();
    }

    public partial class MainWindow : Window
    {
        //MainWindow cur;
        public MainWindow()
        {
            InitializeComponent();
            MyConvert.cur = this as MainWindow;
        }
        public void Convert()
        {

            //tbOutput.Text = "";
            if (myWin.IsLoaded)
            {
                double input = 0;
                string inputStr = tbInput.Text;
                tbOutput.Text = tbInput.Text;

                if (inputStr != "" && !double.TryParse(inputStr, out input))
                {
                    MessageBox.Show("Invalid number");
                    if (inputStr.Length > 0)
                    {
                        tbInput.Text = inputStr.Substring(0, inputStr.Length - 1);
                        tbInput.SelectionStart = inputStr.Length - 1;
                    }
                }
                else
                {
                    //celcius
                    if (rbL1.IsChecked == true && rbR2.IsChecked == true)
                    {
                        tbOutput.Text = string.Format("{0:0.##}{1}F", input * 9.0 / 5 + 32, (char)176); // alt 176, +""+(char)176+"c";
                    }
                    if (rbL1.IsChecked == true && rbR3.IsChecked == true)
                    {
                        tbOutput.Text = string.Format("{0:0.##}{1}K", input + 273.15, (char)176);
                    }

                    //Farenhait
                    if (rbL2.IsChecked == true && rbR1.IsChecked == true)
                    {
                        tbOutput.Text = string.Format("{0:0.##}{1}C", (input - 32) * 5.0 / 9, (char)176); // alt 176, +""+(char)176+"c";
                    }
                    if (rbL2.IsChecked == true && rbR3.IsChecked == true)
                    {
                        tbOutput.Text = string.Format("{0:0.##}{1}K", (input - 32) * 5.0 / 9 + 273.15, (char)176);
                    }

                    //Kevin
                    if (rbL3.IsChecked == true && rbR1.IsChecked == true)
                    {
                        tbOutput.Text = string.Format("{0:0.##}{1}C", (input - 273.15), (char)176); // alt 176, +""+(char)176+"c";
                    }
                    if (rbL3.IsChecked == true && rbR2.IsChecked == true)
                    {
                        tbOutput.Text = string.Format("{0:0.##}{1}F", (input - 273.15) * 9 / 5 + 32, (char)176);
                    }
                }
            }
        }
        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            Convert();
        }

        private void StackPanel_MouseUp(object sender, MouseButtonEventArgs e)

        {
            Convert();
        }

        private void rb_Checked(object sender, RoutedEventArgs e)
        {
            Convert();
        }
    }
}
