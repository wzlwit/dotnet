﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2PeopleTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }
    class PersonTravels
    {
        string name; // 2-50
        int age; // 0-150
        string passportNo; // two uppercase letters, six digits exactly - use regexp
        List<string> countriesVisited = new List<string>();
        public int GetVNum()
        {
            return countriesVisited.Count;
        }

        public List<string> CountriesVisited
        {
            get
            {
                return countriesVisited;
            }
            set
            {
                countriesVisited = value;
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value.Length < 2 || value.Length > 150)
                {
                    throw new InvalidDataException("Name must between 2 and 150!");
                }
                else
                {
                    name = value;
                }
            }


        }

        public int Age
        {
            get { return age; }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new InvalidDataException("Age must between 0 and 150!");
                }
            }
        }

        public string PassportNo
        {
            get { return passportNo; }
            set
            {
                const string PassReg = @"^[A-Z]{2}[0-9]{6}$";
                if (Regex.Match(value, PassReg).Success)
                {
                    passportNo = value;
                }
                else
                {
                    throw new InvalidDataException("Passport must have 2 upper letter followed by 6 digits");
                }
            }
        }

        public PersonTravels(string name, int age, string passportNo, string country)
        {
            Name = name;
            Age = age;
            PassportNo = passportNo;
            string[] token = country.Split(',');
            foreach (string str in token)
            {
                AddCountry(str);
            }
        }

        public string GetVisited()
        {
            string visited = "";
            int len = countriesVisited.Count;
            for (int i = 0; i < len; i++)
            {
                visited += countriesVisited[i] + (i < len ? ", " : "");
            }
            return visited;
        }

        public void AddCountry(String country)
        {
            if (country == "" || country == null)
            {
                Console.WriteLine("Empty country name, ignored!");
            }
            else if (countriesVisited.Contains(country))
            {
                Console.WriteLine("{} already exists, ignored!");
            }
            else
            {
                countriesVisited.Add(country);
            }
        }

        public override string ToString()
        {
            return String.Format("{0} is {1} y/o passport {2} visitied: {3}", Name, Age, PassportNo, GetVisited());
        }
    }
    class Program
    {
        const string FPath = @"..\..\data.txt"; // file path

        static List<PersonTravels> people = new List<PersonTravels>();

        //helper methods:
        static void Display()
        {
            people.ForEach(Console.WriteLine);
        }
        static void AddOne()
        {
            string name, passportNo, country = "";
            int age;
            while (true)
            {
                Console.WriteLine("Enter name:");
                name = Console.ReadLine().Trim();
                if (name == "")
                {
                    Console.WriteLine("invalid name, try again"); continue;
                }
                else { break; }


            }

            while (true)
            {
                Console.WriteLine("Enter age:");
                if (!int.TryParse(Console.ReadLine().Trim(), out age))
                {
                    Console.WriteLine("invalid age, try again"); continue;
                }
                else { break; }
            }

            while (true)
            {
                Console.WriteLine("Enter passport:");
                passportNo = Console.ReadLine().Trim();
                if (passportNo == "")
                {
                    Console.WriteLine("empty passport, try again"); continue;
                }
                else { break; }
            }
            while (true)
            {
                Console.WriteLine("Enter countries one by one (empty enter to escape):");
                string input = Console.ReadLine().Trim();
                if (country == "")
                {
                    break;
                }
                else { country += (input + ","); }
            }

            people.Add(new PersonTravels(name, age, passportNo, country));
        }
        static void Register()
        {
            int flag = people.Count;
            if (flag == 0)
            {
                Console.WriteLine("no records, please add one first");

            }
            else
            {
                for (int i = 0; i < flag; i++)
                {
                    Console.Write(i + 1 + " ");
                    Console.WriteLine(people[i]);
                }
                while (true)
                {

                    Console.Write("enter your choice: ");
                    string choice = Console.ReadLine().Trim();
                    int index;
                    if (!int.TryParse(choice, out index))
                    {
                        Console.WriteLine("not a number!"); continue;
                    }
                    else
                    {
                        if (index > 0 && index < flag + 1)
                        {
                            while (true)
                            {
                                string country = "";
                                Console.Write("Enter one country: ");
                                string input = Console.ReadLine().Trim();
                                if (input == "")
                                {
                                    Console.WriteLine("empty country, try again!");
                                    continue;
                                }
                                else { people[index - 1].AddCountry(input); }
                            }
                        }
                        else
                        {
                            Console.WriteLine("invalid number!"); continue;
                        }
                    }

                }
            }
        }
        static void VisitMost()
        {
            Console.WriteLine("the one visit most countries: ");
            int num = 0;
            for (int i = 1; i < people.Count; i++)
            {
                if (people[i].GetVNum() > people[num].GetVNum()) { num = i; }
            }
            Console.WriteLine(people[num]);

        }

        public static void ReadData()
        {
            try
            {
                string[] linesAry = File.ReadAllLines(FPath);

                string name = "", passportNo = "";
                int age = -1;

                for (int i = 0; i < linesAry.Length; i++)
                {
                    try
                    {
                        string[] line = linesAry[i].Split(';');

                        if (line.Length < 3 || line.Length > 4)
                        {
                            Console.WriteLine("Line {0}: incorrect number of data fields! Skipped!"); continue;
                        }
                        else
                        {
                            name = line[0].Trim();
                            if (name == "")
                            {
                                Console.WriteLine("Line {0}: name is empty!. Skipped!"); continue;
                            }
                            if (!int.TryParse(line[1].Trim(), out age))
                            {
                                Console.WriteLine("Line {0}: not an integer for age! Skipped!"); continue;
                            }
                            passportNo = line[2].Trim();
                            if (passportNo == "")
                            {
                                Console.WriteLine("Line {0}: empty passport number! Skipped!"); continue;
                            }
                            if (line.Length == 3)
                            {
                                people.Add(new PersonTravels(name, age, passportNo, ""));
                            }
                            else
                            {
                                people.Add(new PersonTravels(name, age, passportNo, (line[3].Trim())));
                            }

                        }
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Invalid file, nothing get from the file!");
            }

        }

        public static void WhoVisit()
        {
            Console.WriteLine("Enter a country name: ");
            string ctry = Console.ReadLine().Trim();
            foreach (PersonTravels e in people)
            {
                if (e.CountriesVisited.Contains(ctry))
                {
                    Console.WriteLine(e);
                }
            }
        }

        static void Main(string[] args)
        {

            try
            {
                ReadData();
                while (true)
                {
                    Console.WriteLine("\nMenu:" +
                        "\n1. Display all people and countries they traveled to" +
                        "\n2.Add person and countries visited" +
                        "\n3.Register a new country visited by a person" +
                        "\n4.Find out who visited most countries" +
                        "\n5.List all who visited a specific country" +
                        "\n0.Exit");


                    Console.Write("Enter your choice: ");

                    string choice = Console.ReadLine().Trim();
                    switch (choice)
                    {
                        case "1":
                            {
                                Display();
                                break;
                            }
                        case "2":
                            {
                                AddOne();
                                break;
                            }
                        case "3":
                            {
                                Register();
                                break;
                            }
                        case "4":
                            {
                                VisitMost();
                                break;
                            }
                        case "5":
                            {
                                WhoVisit();
                                break;
                            }
                        case "0":
                            {
                                Environment.Exit(1);
                                break;
                            }
                        default:
                            {
                                Console.Write("Invalid choice, try again");
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("unknow error! Enter any key to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }

        }
    }
}

