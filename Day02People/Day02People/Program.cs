﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02People
{
    class Program
    {
        static string[] namesArray = new string[4];
        static int[] agesArray = new int[4];

        static void Main(string[] args)
        {
            // ask for entries
            for (int i = 0; i < namesArray.Length; i++)
            {
                string name;
                int age;

                // get name
                while (true)
                {
                    Console.Write("Enter user name No.{0}: ", i+1);
                    name = Console.ReadLine().Trim();
                    if (string.Compare(name, "") != 0)
                    {
                        namesArray[i] = name;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("not a valid name, please try again:");
                    }
                }

                // get age
                while (true)
                {
                    Console.Write("Enter user age No.{0}: ", i+1);
                    if (int.TryParse(Console.ReadLine(), out age))
                    {
                        agesArray[i] = age;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("not a valid integer, please try again:");
                    }
                }
            }


            // find the younges
            int youngest = 0;
            for (int i = 1; i < agesArray.Length; i++)
            {
                if (agesArray[i] < agesArray[youngest])
                {
                    youngest = i;
                }
            }

            Console.WriteLine("Youngest person is {0} and the name is {1}", agesArray[youngest], namesArray[youngest]);

            Console.ReadKey();
        }
    }
}
