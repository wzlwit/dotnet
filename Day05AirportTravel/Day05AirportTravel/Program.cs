﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day05AirportTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }
    class Airport
    {
        string regex = @"^[A-Z]{3}$";

        String _code, _city;
        GeoCoordinate _coordinate = new GeoCoordinate();

        public Airport(string code, string city, double latitude, double longitude)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = longitude;
        }

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (Regex.Match(value.Trim(), this.regex).Success)
                {
                    _code = value.Trim();
                }
                else
                {
                    throw new InvalidDataException("The code for airport is not correct");
                }
            }
        }
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Trim() != "")
                {
                    _city = value;
                }
                else
                {
                    throw new InvalidDataException("City is empty");
                }
            }
        }

        public double Latitude
        {
            get
            {
                return _coordinate.Latitude;
            }
            set
            {
                if (value == -180)
                {
                    _coordinate.Latitude = -value;

                }
                else if (value > -180 && value <= 180)
                {
                    _coordinate.Latitude = value;
                }
                else
                {
                    throw new InvalidDataException("Latitude must be btween -180 and 180!");
                }
            }
        }
        public double Longitude
        {
            get
            {
                return _coordinate.Longitude;
            }
            set
            {
                if (value >= -90 && value <= 90)
                {
                    _coordinate.Longitude = value;
                }
                else
                {
                    throw new InvalidDataException("Longitude must be btween -90 and 90!");
                }
            }
        }
        public GeoCoordinate GetCoord()
        {
            return _coordinate;
        }
        public override string ToString()
        {
            return String.Format("Airport {0} in {1} with latitude {2} and longitude{3}", Code, City, Latitude, Longitude);
        }
    }
    class Program
    {
        const string FPath = @"../../airports.txt";
        static bool EmptyLastLine = false;
        static int totalLineNum = 0;
        static int validLineNum = 0;
        static List<Airport> airports = new List<Airport>();

        static void ReadAllAiports()
        {
            try
            {
                //string file = File.ReadAllText(FPath);
                string[] lines = File.ReadAllLines(FPath);

                //string[] lines = file.Split('\n');
                totalLineNum = lines.Length;
                if (totalLineNum < 1)
                {
                    throw new IOException("File is empty!");
                }
                if (lines[totalLineNum - 1].Trim() == "")
                {
                    EmptyLastLine = true;
                    Console.WriteLine("there is empty line at the end!");
                }

                for (int i = 0; i < lines.Length; i++)
                {
                    try
                    {
                        string[] tokens = lines[i].Split(';');
                        double latitiude, longitude;



                        if (tokens.Length != 4)
                        {
                            Console.WriteLine("File line {0}: wrong amount of fields, the line is ignored!", i + 1);
                        }
                        else
                        {
                            if (!double.TryParse(tokens[2].Trim(), out latitiude))
                            {
                                throw new ArgumentException(String.Format("invalid number for latitude, the line is ignored!", i + 1));
                            }
                            if (!double.TryParse(tokens[3].Trim(), out longitude))
                            {
                                throw new ArgumentException(String.Format("invalid number for longitude, the line is ignored!", i + 1));
                            }
                            airports.Add(new Airport(tokens[0], tokens[1], latitiude, longitude));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("File line " + (i + 1) + ": " + ex.GetType() + ": " + ex.Message);
                    }

                }
                validLineNum = airports.Count; //record the last index after reading data

            }
            catch (Exception ex)
            {
                if (ex is FileNotFoundException)
                {
                    Console.WriteLine("File not found!");
                }
                else if (ex is IOException)
                {
                    Console.WriteLine("{0}: There is something wrong with the file ({1})!", ex.GetType(), ex.Message);
                }

            }
            finally
            {
                Console.Write("Press any key to continue!\n");
                Console.ReadKey();
            }
        }

        static void ShowMenu()
        {
            Console.WriteLine("\n" +
                "1. Show all airports (codes and city names)\n" +
                "2. Find distance between two airports by codes.\n" +
                "3. Find the airports nearest to an airport given by code and display the distance.\n" +
                "4. Add a new airport to the list.0.Exit.\n"
                    );
        }

        static int GetChoice()
        {
            string choice = "";
            while (true)
            {
                Console.Write("Enter your choice:");
                choice = Console.ReadLine().Trim();
                switch (choice)
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "0":
                        {
                            return int.Parse(choice);
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice, try again!");
                            break;
                        };
                }
            }
        }

        static void ShowAirports()
        {
            airports.ForEach(Console.WriteLine);
        }

        static Airport GetByCode()
        {
            Airport found = null;

            while (true)
            {
                Console.WriteLine("Enter the code");
                string code = Console.ReadLine();
                foreach (Airport e in airports)
                {
                    if (e.Code == code)
                    {
                        found = e;
                        break;
                    }

                }
                if (found == null)
                {
                    Console.WriteLine("Code not found, try again");
                }
                else
                {
                    break;
                }

            }
            return found;
        }

        static void Distance()
        {
            Airport a1 = null, a2 = null;
            Console.WriteLine("Enter code for the first Airport： ");
            a1 = GetByCode();
            Console.WriteLine("Enter code for the second Airport： ");
            a2 = GetByCode();
            Console.WriteLine("The distance between these two airports are {0} meters!",
                a1.GetCoord().GetDistanceTo(a2.GetCoord())
                );
        }

        static void Nearest()
        {
            Airport concerned = GetByCode();
            double distance = double.MaxValue;
            int index = 0;
            for (int i = 0; i < airports.Count; i++)
            {
                if (airports[i] != concerned)
                {
                    double dis = airports[i].GetCoord().GetDistanceTo(concerned.GetCoord());
                    if (dis < distance)
                    {
                        index = i;
                        distance = dis;
                    }
                }
            }
            Console.WriteLine("The neareast airport to {0} is {1}", concerned.Code, airports[index].Code);
        }

        static void AddAirport()
        {
            string regex = @"^[A-Z]{3}$";
            string code, city;
            double latitude, longitude;
            while (true)
            {
                Console.Write("Enter the code: ");
                code = Console.ReadLine().Trim();
                if (code == "" || !Regex.Match(code, regex).Success)
                {
                    Console.WriteLine("Code must be three uppcased letters!");
                }
                else { break; }
            }
            while (true)
            {
                Console.Write("Enter the city: ");
                city = Console.ReadLine().Trim();
                if (city == "")
                {
                    Console.WriteLine("City must not be empty!");
                }
                else { break; }
            }

            while (true)
            {
                Console.Write("Enter the latitude: ");
                if (double.TryParse(Console.ReadLine().Trim(), out latitude))
                {
                    if (latitude >= -90 && latitude <= 90)
                    {
                        break;
                    }
                    {
                        Console.WriteLine("latitude must be between -90 and 90!"); 
                    }
                }else
                {
                    Console.WriteLine("Invalid number!");                
                }
                
            }
            while (true)
            {
                Console.Write("Enter the longitude: ");
                if (double.TryParse(Console.ReadLine().Trim(), out longitude))
                {
                    if (longitude >= -180 && longitude <= 180)
                    {
                        break;
                    }
                    {
                        Console.WriteLine("longitude must be between -180 and 180!"); 
                    }
                }else
                {
                    Console.WriteLine("Invalid number!");                
                }
                
            }

            
            airports.Add(new Airport(code, city, latitude, longitude));

        }

        //* main 
        static void Main(string[] args)
        {
            int choice = 0;
            ReadAllAiports();

            do
            {
                try
                {
                    ShowMenu();
                    choice = GetChoice();
                    switch (choice)
                    {
                        case 1: { ShowAirports(); break; }
                        case 2: { Distance(); break; }
                        case 3: { Nearest(); break; }
                        case 4: { AddAirport(); break; }
                        case 0:
                            {
                                if (airports.Count > validLineNum)
                                {

                                    if (!EmptyLastLine) { File.AppendAllText(FPath, "\n"); }
                                    string[] newLines = new string[airports.Count - validLineNum - 1];
                                    Airport ap = null;
                                    for (int i = validLineNum; i < airports.Count; i++)
                                    {
                                        ap = airports[i];
                                        int j = i - validLineNum;
                                        newLines[j] = ap.Code + ";" + ap.City + ";" + ap.Latitude + ";" + ap.Longitude;
                                    }
                                    File.AppendAllLines(FPath, newLines);

                                }
                                Console.WriteLine("Good bye!");
                                Console.ReadLine();
                                Environment.Exit(1);
                                break;
                            }
                        default: { Console.Write("Unknown internal error!"); Console.ReadLine(); break; }
                    }
                    Console.ReadKey();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.GetType() + ": " + ex.Message);
                }
                finally
                {
                    Console.WriteLine("\nPress any key to run again!");
                    Console.ReadKey();
                }
            }
            while (choice != 0);
        }
    }
}
